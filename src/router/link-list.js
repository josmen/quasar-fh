export const linksList = [
  {
    title: "Typography",
    caption: "Tipos de letras en Quasar",
    icon: "las la-font",
    link: "typography",
  },
  {
    title: "Flex Layout",
    caption: "Estilos con Flex",
    icon: "las la-layer-group",
    link: "flex",
  },
  {
    title: "Docs",
    caption: "quasar.dev",
    icon: "las la-graduation-cap",
    link: "https://quasar.dev",
  },
];
